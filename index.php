<?php
require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");
echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br><br>";// false

$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->legs . "<br>"; 
$sungokong->yell(); // "Auooo"
echo "<br>"; 

$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->legs . "<br>"; 
$kodok->jump() ; // "hop hop"

?>